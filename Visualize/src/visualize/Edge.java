package visualize;

public class Edge implements Comparable<Edge> {

  private static int numberOfEdges;
  private final int id;
  private final Vertex endPoint0;
  private final Vertex endPoint1;
  private final double length;

  public Edge(Vertex endPoint0, Vertex endPoint1) {
    this.id = Edge.numberOfEdges;
    Edge.numberOfEdges++;
    this.endPoint0 = endPoint0;
    this.endPoint1 = endPoint1;
    double xDiff = endPoint0.getX() - endPoint1.getX();
    double yDiff = endPoint0.getY() - endPoint1.getY();
    this.length = Math.sqrt(xDiff * xDiff + yDiff * yDiff);
  } // Edge( Vertex, Vertex )

  @Override
  public boolean equals(Object o) {
    boolean result = false;
    if (o instanceof Edge) {
      Edge e = (Edge) o;
      result = this.getId() == e.getId();
    } // if
    return result;
  } // equals( Object)

  @Override
  public int hashCode() {
    return (new Integer(this.getId())).hashCode();
  } // hashCode()

  public int getId() {
    return this.id;
  } // getId()

  public Vertex getEndPoint0() {
    return this.endPoint0;
  } // getEndPoint0()

  public Vertex getEndPoint1() {
    return this.endPoint1;
  } // getEndPoint1()

  public double getLength() {
    return this.length;
  } // getLength()

  /** Return the distance between the line segment a point.
   * 
   * 
   * @param v is a point.
   * @return the distance from the line that is defined by the endpoints
   * of the edge if the point that is closest to v is on the edge (within
   * the line segment defined by the endpoints) and return infinity otherwise.
   */
  public double distanceTo( Vertex v ) {
    double x0 = this.getEndPoint0().getX();
    double y0 = this.getEndPoint0().getY();
    
    double x1 = this.getEndPoint1().getX();
    double y1 = this.getEndPoint1().getY();
    
    double x2 = v.getX();
    double y2 = v.getY();
    
    double numerator = (x0 - x1) * (x0 - x2) + (y0 - y1) * (y0 - y2);
    double denominator = (x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0);
    double s = numerator/denominator;
    
    double distance = Double.POSITIVE_INFINITY;
    
    if( (0 <= s) && (s <= 1)) {
      double x = x0 + s * (x1 - x0);
      double y = y0 + s * (y1 - y0);
      
      double xDiff = x2 - x;
      double yDiff = y2 - y;
      
      distance = Math.sqrt( xDiff * xDiff + yDiff * yDiff);
    } // if
    
    return distance;
  } // distanceTo( Vector )
  
  public boolean intersects(Edge e) {
    return intersects(e.getEndPoint0(), e.getEndPoint1());
  } // intersects( Edge )

  public boolean intersects(Vertex v0, Vertex v1) {

    Vertex u0 = this.getEndPoint0();
    Vertex u1 = this.getEndPoint1();

    boolean endPointInCommon = (u0.equals(v0))
            || (u0.equals(v1))
            || (u1.equals(v0))
            || (u1.equals(v1));

    if (endPointInCommon) {
      return false;
    } // if
    else {
      double x0 = u0.getX();
      double y0 = u0.getY();

      double x1 = u1.getX();
      double y1 = u1.getY();

      double x2 = v0.getX();
      double y2 = v0.getY();

      double x3 = v1.getX();
      double y3 = v1.getY();

      double a = (x2 - x3);
      double b = (x1 - x0);

      double c = (y2 - y3);
      double d = (y1 - y0);

      double rhs0 = (x2 - x0);
      double rhs1 = (y2 - y0);

      double determinant = a * d - b * c;
      double s = (d * rhs0 - b * rhs1) / determinant;
      double t = (-c * rhs0 + a * rhs1) / determinant;

      boolean sInBounds = (0 < s) && (s < 1);
      boolean tInBounds = (0 < t) && (t < 1);

      return sInBounds && tInBounds;
    } // else
  } // intersects( Vertex v0, Vertex v1 )

  @Override
  public String toString() {
    return String.format( "[edge #%2d:%s,%s]", this.getId(), 
            this.getEndPoint0(), this.getEndPoint1());
  } // toString()

  @Override
  public int compareTo(Edge e) {
    if (this.getLength() < e.getLength()) {
      return -1;
    } // if
    else if (this.getLength() > e.getLength()) {
      return +1;
    } // else if
    else {
      return 0;
    } // else
  } // compareTo( Edge )
} // Edge
