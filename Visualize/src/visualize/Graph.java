package visualize;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Graph {

  private static final double MINIMUM_SEPARATION = 0.2;
  private final Random rng;
  private final List<Vertex> vertices;
  private final List<Edge> edges;

  public Graph(int numberOfVertices) {
    this.rng = new Random();
    this.vertices = new ArrayList<>();
    this.edges = new ArrayList<>();

    int count = 0;
    while (count < numberOfVertices) {
      double x = 2 * rng.nextDouble() - 1;
      double y = 2 * rng.nextDouble() - 1;

      boolean accept = true;
      for (Vertex v : this.vertices) {
        accept = accept && v.distanceTo(x, y) > MINIMUM_SEPARATION;
      } // for

      if (accept) {
        Vertex v = new Vertex(x, y);
        this.vertices.add(v);
        count++;
      } // if

    } // while

    List<Edge> candidateEdges = new ArrayList<>();
    for (int i = 0; i < this.vertices.size(); i++) {
      for (int j = i + 1; j < this.vertices.size(); j++) {
        Vertex v0 = vertices.get(i);
        Vertex v1 = vertices.get(j);

        Edge e = new Edge(v0, v1);

        candidateEdges.add(e);
      } // for
    } // for

    Collections.shuffle(candidateEdges);

    for (Edge e : candidateEdges) {
      boolean foundIntersection = false;
      Iterator<Edge> edgeIterator = this.edges.iterator();
      while (edgeIterator.hasNext() && !foundIntersection) {
        Edge otherEdge = edgeIterator.next();
        if (e.getId() != otherEdge.getId()) {
          foundIntersection = e.intersects(otherEdge);
        } // if
      } // while
      
      boolean tooClose = false;
      Iterator<Vertex> vertexIterator = this.vertices.iterator();
      while( vertexIterator.hasNext() && !tooClose ) {
        Vertex v = vertexIterator.next();
        boolean isEndPoint = v.equals(e.getEndPoint0()) ||
                v.equals(e.getEndPoint1());
        
        if( !isEndPoint && e.distanceTo(v) < MINIMUM_SEPARATION ) {
          tooClose = true;
        } // if
      } // while
      
      if (!foundIntersection && !tooClose) {
        this.addEdge(e);
      } // if
    } // for

  } // Graph( int )

  public List<Vertex> getVertices() {
    return this.vertices;
  } // getVertices()

  public List<Edge> getEdges() {
    return this.edges;
  } // getEdges()
  
  public final void addEdge( Edge e ) {
    this.edges.add(e);
    e.getEndPoint0().addEdge(e);
    e.getEndPoint1().addEdge(e);
  } // addEdge( Edge )
  
  public void print() {
      System.out.format( "%4d\n%4d\n", this.vertices.size(),
              this.edges.size());
      for( Edge e : this.edges ) {
          System.out.format( "%4d %4d %8.4f\n", 
                  e.getEndPoint0().getId(),
                  e.getEndPoint1().getId(),
                  e.getLength());
      } // for
  } // print()
  
} // Graph
