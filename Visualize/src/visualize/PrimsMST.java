package visualize;

import java.util.HashSet;
import java.util.PriorityQueue;

public class PrimsMST extends Graph {

    private HashSet<Vertex> treeVertices;
    private PriorityQueue<Edge> candidateEdges;
    private HashSet<Edge> treeEdges = new HashSet<>();

    public PrimsMST(int numberOfVertices) {
        super(numberOfVertices);
        treeVertices = new HashSet<>();
        candidateEdges = new PriorityQueue<>();

        Vertex v = this.getVertices().get(0);
        System.out.println("Look for edges that are "
                + "connected to vertex #" + v.getId());
        for (Edge e : v.getEdges()) {
            System.out.println("\tAdd edge #" + e.getId()
                    + " to the list of candidate edges.");
            candidateEdges.add(e);
        } // for
        treeVertices.add(v);

        int count = 0;
        while (count < 8 && treeEdges.size() < getVertices().size() - 1) {
            Edge nextTreeEdge = candidateEdges.remove();

            Vertex v0 = nextTreeEdge.getEndPoint0();
            Vertex v1 = nextTreeEdge.getEndPoint1();

            boolean isV0 = (treeVertices.contains(v0)
                    && !treeVertices.contains(v1));
            boolean isV1 = (!treeVertices.contains(v0)
                    && treeVertices.contains(v1));

            assert isV0 || isV1 : "One vertex must already be "
                    + "part of the tree and the other must not.";

            if (isV0) {
                v = v0;
            } // if
            else if (isV1) {
                v = v1;
            } // else

            if (isV0 || isV1) {
                System.out.println("Look for edges that are "
                        + "connected to vertex #" + v.getId());

                for (Edge e : v.getEdges()) {
                    if (!candidateEdges.contains(e)) {
                        System.out.println("\tAdd edge #" + e.getId()
                                + " to the list of candidate edges.");
                        candidateEdges.add(e);
                    } // if
                } // for
                treeVertices.add(v);

                System.out.println("Add edge #" + nextTreeEdge.getId());

                treeEdges.add(nextTreeEdge);
            } // if
            count++;
        } // while

    } // PrimsMST( int )

    public HashSet<Edge> getTreeEdges() {
        return treeEdges;
    } // getTreeEdges()
} // PrimsMST
