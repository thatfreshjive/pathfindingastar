package visualize;

import java.util.ArrayList;
import java.util.List;

public class Vertex {

  private static int numberOfVertices;
  private final int id;
  private final double x;
  private final double y;
  private final List<Edge> edges;

  public Vertex(double x, double y) {
    this.id = Vertex.numberOfVertices;
    Vertex.numberOfVertices++;
    this.x = x;
    this.y = y;
    this.edges = new ArrayList<>();
  } // Vertex( double, double )

  @Override
  public boolean equals(Object o) {
    boolean result = false;
    if (o instanceof Vertex) {
      Vertex v = (Vertex) o;
      result = this.getId() == v.getId();
    } // if
    return result;
  } // equals( Object)

  @Override
  public int hashCode() {
    return (new Integer(this.getId())).hashCode();
  } // hashCode()

  public int getId() {
    return this.id;
  } // getId()

  public double getX() {
    return this.x;
  } // getX()

  public double getY() {
    return this.y;
  } // getY()

  public List<Edge> getEdges() {
    return this.edges;
  } // getEdges()

  public void addEdge(Edge e) {
    this.edges.add(e);
  } // addEdge( e )

  public double distanceTo(Vertex v) {
    double deltaX = this.getX() - v.getX();
    double deltaY = this.getY() - v.getY();
    return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
  } // distanceTo( Vertex )

  public double distanceTo(double x, double y) {
    double deltaX = this.getX() - x;
    double deltaY = this.getY() - y;
    return Math.sqrt(deltaX * deltaX + deltaY * deltaY);
  } // distanceTo( double, double )

  @Override
  public String toString() {
    return String.format("(vertex #%2d: x=%6.2f, y=%6.2f)", this.getId(),
            this.getX(), this.getY());
  } // toString()
} // Vertex
