package visualize;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class Visualize extends JFrame implements ActionListener {

  private final int FRAME_WIDTH = 512;
  private final int FRAME_HEIGHT = 512;
  private final String FRAME_TITLE = "Visualize Graph";
  private final int NUMBER_OF_VERTICES = 8;

  private final VisualizePanel panel;
  private final HashMap<String, Integer> graphs = new HashMap<>();

  public Visualize() {
    this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
    this.setTitle(FRAME_TITLE);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    Container pane = this.getContentPane();
    Graph graph = new Graph(NUMBER_OF_VERTICES);
    this.panel = new VisualizePanel(graph);
    pane.add(this.panel);

    JMenuBar menuBar = new JMenuBar();
    JMenu sizeMenu = new JMenu("Size");

    for (int i = 6; i <= 16; i += 2) {
      String s = String.format("%2d vertices", i);
      graphs.put(s, i);
      JMenuItem menuItem = new JMenuItem(s);
      menuItem.setActionCommand(s);
      sizeMenu.add(menuItem);
      menuItem.addActionListener(this);
    } // for

    menuBar.add(sizeMenu);
    this.setJMenuBar(menuBar);

    this.setVisible(true);
  } // Visualize()

  public static void main(String[] args) {
    Visualize visualize = new Visualize();
  } // main( String [] )

  @Override
  public void actionPerformed(ActionEvent e) {
      String s = e.getActionCommand();
      Graph graph = new Graph(graphs.get(s));
      graph.print();
      this.panel.setGraph(graph);
//    System.out.println( "Hi");
//    String s = e.getActionCommand();
//    PrimsMST graph = new PrimsMST( graphs.get(s));
//    this.panel.setGraph(graph);
  } // actionPerformed( ActionEvent )

} // Visualize
