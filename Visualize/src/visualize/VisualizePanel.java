package visualize;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.Collections;
import javax.swing.JPanel;

public class VisualizePanel extends JPanel {

    private static final Color BG_COLOR = new Color(172, 206, 248);
    private static final Color VERTEX_COLOR = new Color(124, 106, 102);
    private static final Color EDGE_COLOR = new Color(112, 132, 120);

    private static final double HORIZONTAL_MARGIN = 0.1;
    private static final double VERTICAL_MARGIN = 0.1;
    private static final double DOT_RADIUS = 0.02;
    private Graph graph;

    public VisualizePanel(Graph graph) {
        this.graph = graph;
        this.setBackground(BG_COLOR);
    } // VisualizePanel()

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2D = (Graphics2D) g;

        AffineTransform translate = new AffineTransform();
        translate.setToTranslation(1.0, 1.0);

        AffineTransform scaleToMargins = new AffineTransform();
        double xFactor = 1.0 - HORIZONTAL_MARGIN;
        double yFactor = 1.0 - VERTICAL_MARGIN;
        scaleToMargins.setToScale(xFactor, yFactor);

        AffineTransform scaleToWindow = new AffineTransform();
        scaleToWindow.setToScale(this.getWidth() / 2, this.getHeight() / 2);

        AffineTransform transform = new AffineTransform();
        transform.concatenate(scaleToWindow);
        transform.concatenate(translate);
        transform.concatenate(scaleToMargins);

        Stroke stroke = new BasicStroke(3);
        g2D.setStroke(stroke);
        g2D.setColor(EDGE_COLOR);
        for (Edge e : this.graph.getEdges()) {
            Vertex v0 = e.getEndPoint0();
            Vertex v1 = e.getEndPoint1();
            double x0 = v0.getX();
            double y0 = v0.getY();
            double x1 = v1.getX();
            double y1 = v1.getY();
            Line2D line = new Line2D.Double(x0, y0, x1, y1);
            g2D.draw(transform.createTransformedShape(line));
        } // for

//        if (this.graph instanceof PrimsMST) {
//            g2D.setColor(Color.RED);
//            PrimsMST mst = (PrimsMST) graph;
//            for (Edge e : mst.getTreeEdges()) {
//                Vertex v0 = e.getEndPoint0();
//                Vertex v1 = e.getEndPoint1();
//                double x0 = v0.getX();
//                double y0 = v0.getY();
//                double x1 = v1.getX();
//                double y1 = v1.getY();
//                Line2D line = new Line2D.Double(x0, y0, x1, y1);
//                g2D.draw(transform.createTransformedShape(line));
//            } // for
//        } // if
        
        g2D.setColor(VERTEX_COLOR);
        for (Vertex v : this.graph.getVertices()) {
            Shape dot = this.makeDot(v);
            g2D.fill(transform.createTransformedShape(dot));
        } // for
    } // paintComponent( Graphics )

    public void setGraph(Graph graph) {
        this.graph = graph;
        this.repaint();
    } // setGraph( Graph )

    private Shape makeDot(Vertex v) {
        double ulx = v.getX() - DOT_RADIUS;
        double uly = v.getY() - DOT_RADIUS;
        double diameter = 2 * DOT_RADIUS;
        Ellipse2D dot
                = new Ellipse2D.Double(ulx, uly, diameter, diameter);
        return dot;
    } // makeDot( Vertex )
} // VisualizePanel
