/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package astar;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Graph {

		private int V;
		private int E;
		private Bag<Edge>[] adj;

		public Graph(String filename) throws FileNotFoundException, IOException {
				this.V = 0;
				this.E = 0;
				FileInputStream fstream = new FileInputStream(filename);
				BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

				String line;
				while ((line = br.readLine()) != null) {
						String[] splitLine = line.split(" ");
						if (splitLine.length == 1) {
								if (this.V == 0 || this.E == 0) {
										this.V = Integer.parseInt(splitLine[0]);
								} else {
										this.E = Integer.parseInt(splitLine[0]);
								}
						}
						int v = Integer.parseInt(splitLine[0]);
						int w = Integer.parseInt(splitLine[1]);
						double weight = Double.parseDouble(splitLine[2]);
						Edge edge = new Edge(v, w, weight);
						addEdge(edge);
				}

		}

		public int V() {
				return V;
		}

		public int E() {
				return E;
		}

		// throw an IndexOutOfBoundsException unless 0 <= v < V
		private void validateVertex(int v) {
				if (v < 0 || v >= V) {
						throw new IndexOutOfBoundsException("vertex " + v + " is not between 0 and " + (V - 1));
				}
		}

		/**
		 * Adds the undirected edge <tt>e</tt> to the edge-weighted graph.
		 *
		 * @param e the edge
		 * @throws java.lang.IndexOutOfBoundsException unless both endpoints are
		 * between 0 and V-1
		 */
		public void addEdge(Edge e) {
				int v = e.either();
				int w = e.other(v);
				validateVertex(v);
				validateVertex(w);
				adj[v].add(e);
				adj[w].add(e);
				E++;
		}

		/**
		 * Returns the edges incident on vertex <tt>v</tt>.
		 *
		 * @return the edges incident on vertex <tt>v</tt> as an Iterable
		 * @param v the vertex
		 * @throws java.lang.IndexOutOfBoundsException unless 0 <= v < V
		 */
		public Iterable<Edge> adj(int v) {
				validateVertex(v);
				return adj[v];
		}

		/**
		 * Returns the degree of vertex <tt>v</tt>.
		 *
		 * @return the degree of vertex <tt>v</tt>
		 * @param v the vertex
		 * @throws java.lang.IndexOutOfBoundsException unless 0 <= v < V
		 */
		public int degree(int v) {
				validateVertex(v);
				return adj[v].size();
		}

		/**
		 * Returns all edges in the edge-weighted graph. To iterate over the
		 * edges in the edge-weighted graph, use foreach notation:
		 * <tt>for (Edge e : G.edges())</tt>.
		 *
		 * @return all edges in the edge-weighted graph as an Iterable.
		 */
		public Iterable<Edge> edges() {
				Bag<Edge> list = new Bag<Edge>();
				for (int v = 0; v < V; v++) {
						int selfLoops = 0;
						for (Edge e : adj(v)) {
								if (e.other(v) > v) {
										list.add(e);
								} // only add one copy of each self loop (self loops will be consecutive)
								else if (e.other(v) == v) {
										if (selfLoops % 2 == 0) {
												list.add(e);
										}
										selfLoops++;
								}
						}
				}
				return list;
		}
}
