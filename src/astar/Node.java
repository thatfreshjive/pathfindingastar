package astar;

public class Node {

	public int isOpen;
	public boolean isBlocking;
	public Location loc;
	public double G;
	public double H;
	public double F;
	public Node parent;

	public Node(Location loc, int open, boolean blocking){
			this.loc = loc;
			this.isOpen = open;
			this.isBlocking = blocking;
	}

	public Node getParentNode(){
			return this.parent;
	}

	public void setParentNode(Node node){
			this.parent = node;
			this.G = this.parent.G + getEdgeCost(this.loc, this.parent.loc);
	}

	public Node(int x, int y, boolean blocking, Location end){
			this.loc = new Location(x, y);
			this.isOpen = 2;
			this.isBlocking = blocking;
			this.H = getEdgeCost(this.loc, end);
			this.G = 0;
            this.F = this.G + this.H;
	}

	public Node(){

	}

	public static double getEdgeCost(Location start, Location end){
			double deltaX = end.getX() - start.getX();
			double deltaY = end.getY() - start.getY();
			double cost = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
			return cost;
	}

}
