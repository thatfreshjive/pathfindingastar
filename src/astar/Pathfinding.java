package astar;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Pathfinding {

        private Node[][] nodes;
        private boolean[][] map;
        private int width;
        private int height;
        private Location endLocation;
        private Node startNode;
        private Node endNode;
        public PathingGrid grid;
        public JFrame window;

        private void createMap(){
            this.map = new boolean[30][20];
            for(int i = 19; i > 3; i--){
                    this.map[21][i] = true;
                }
            for(int i = 0; i < 10; i++){
                    this.map[14][i] = true;
                }
            for(int i = 9; i < 16; i++){
                    this.map[6][i] = true;
                }
            for(int i = 6; i < 14; i++){
                    this.map[i][9] = true;
                }
        }

        private class NodeSort implements Comparator<Node> {

                @Override
                public int compare(Node n1, Node n2) {
                        int comp = 0;

                        System.out.println(n1.F + " " + n2.F);
                        if (n1.F < n2.F){
                            comp = -1;
                        }
                        if (n1.F > n2.F){
                            comp = 1;
                        }
                        return  comp;
                }
        }

        public ArrayList<Location> findPath(){
                ArrayList<Location> path = new ArrayList<Location>();
                boolean success = Search(this.startNode);
                if(success){
                        Node node = this.endNode;
                        while(node.parent != null){
                            path.add(node.loc);
                            this.grid.fillPathCell(node.loc.getX(), node.loc.getY());
                        }
                }

                return path;


        }

        //http://blog.two-cats.com/2014/06/a-star-example/
        private ArrayList<Node> getAdjacentOpenNodes(Node from_node) {
                ArrayList<Node> openNodes = new ArrayList<Node>();
                ArrayList<Location> adjacentPoints = getAdjacentPoints(from_node.loc);
                System.out.println("adjacent nodes");

                for (Location loc : adjacentPoints) {
                        int x = loc.getX();
                        int y = loc.getY();

                        // grid boundries
                        if (x < 0 || x >= this.width || y < 0 || y >= this.height) {
                            continue;

                        }

                        else {
                                Node node = this.nodes[x][y];
                                // ignore closed or blocked nodes
                                if (!(node.isOpen == 1) || !node.isBlocking) {
                                        continue;
                                }

                                if (node.isOpen == 1 && !node.isBlocking) {
                                        double traversalCost = Node.getEdgeCost(node.loc, node.parent.loc);
                                        double gTemp = from_node.G + traversalCost;
                                        if (gTemp < node.G) {
                                                node.setParentNode(from_node);
                                                openNodes.add(node);
                                                this.grid.fillCell(x, y);
                                        }
                                } else {
                                        // If it's untested, set the parent and flag it as 'Open' for consideration
                                        node.parent = from_node;
                                        node.isOpen = 1;
                                        openNodes.add(node);
                                }
                        }
                }
                return openNodes;
        }

        private ArrayList<Location> getAdjacentPoints(Location loc){
                int x = loc.getX();
                int y = loc.getY();
                ArrayList<Location> adjacent = new ArrayList<Location>();
                adjacent.add(new Location(x+1, y));
                adjacent.add(new Location(x, y+1));
                if (x > 0){
                    adjacent.add(new Location(x-1, y));
                }
                if (y > 0){
                    adjacent.add(new Location(x, y-1));
                }
                System.out.println("points");
                return adjacent;

        }

        private boolean Search(Node currentNode){
                currentNode.isOpen = 0;
                System.out.println("searching");
                ArrayList<Node> nextNodes = getAdjacentOpenNodes(currentNode);
                System.out.println("searching");
                Collections.sort(nextNodes, new NodeSort());
                this.grid.fillCell(currentNode.loc.getX(), currentNode.loc.getY());

                for(Node next : nextNodes){
                        if(next.loc.isEqualTo(this.endNode.loc)){
                            System.out.println("searching true");
                                return true;
                        }
                        else{
                            System.out.println("searching false");
                                if(Search(next)){
                                    return true;
                                }
                        }
                }

                return false;
        }

        private void initNodes(){
                this.width = 30;
                this.height = 20;
                createMap();
                this.endLocation = new Location(this.width-1, this.height-1);
                this.nodes = new Node[this.width][this.height];
                for(int i = 0; i < this.width; i++){
                        for(int j = 0; j < this.height; j++){
                                this.nodes[i][j] = new Node(i, j, this.map[i][j], this.endLocation);
                        }
                }
                this.startNode = this.nodes[0][0];
                this.endNode = this.nodes[this.width-1][this.height-1];

                // Set up the visualization
                this.grid = new PathingGrid();
                this.window = new JFrame();
                this.window.setSize(this.width*30, this.height*30);
                this.window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                this.window.add(this.grid);
                this.window.setVisible(true);
                for(int i = 0; i < this.width; i++){
                        for(int j = 0; j < this.height; j++){
                                if(this.map[i][j]){
                                        this.grid.fillCell(i, j);
                                }
                        }
                }
        }

        public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                }

                Pathfinding finder = new Pathfinding();
                finder.initNodes();
                finder.findPath();

//                grid.fillCell(0, 0);
//                grid.fillCell(79, 0);
//                grid.fillCell(0, 49);
//                grid.fillCell(79, 49);
//                grid.fillCell(39, 24);
            }
        });
        }

}
