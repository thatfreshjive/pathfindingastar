package astar;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class PathingGrid extends JPanel {

		private Color color = Color.RED;

		private List<Point> fillCells;
		private List<Point> astarCells;

		public PathingGrid() {
				fillCells = new ArrayList<>(25);
				astarCells = new ArrayList<>(25);
		}

		@Override
		protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				for (Point fillCell : fillCells) {
						int cellX = 10 + (fillCell.x * 30);
						int cellY = 10 + (fillCell.y * 30);
						g.setColor(Color.BLACK);
						g.fillRect(cellX, cellY, 30, 30);
				}
				for (Point astarCell : astarCells) {
						int cellX = 10 + (astarCell.x * 30);
						int cellY = 10 + (astarCell.y * 30);
						g.setColor(this.color);
						g.fillRect(cellX, cellY, 30, 30);
				}
				g.setColor(Color.BLACK);
				g.drawRect(10, 10, 910, 610);

				for (int i = 10; i <= 910; i += 30) {
						g.drawLine(i, 10, i, 610);
				}

				for (int i = 10; i <= 610; i += 30) {
						g.drawLine(10, i, 910, i);
				}
		}

		public void fillCell(int x, int y) {
				fillCells.add(new Point(x, y));
				repaint();
		}

		public void fillPathCell(int x, int y){
				astarCells.add(new Point(x, y));
				repaint();
		}

		public void setColor(Color color){
				this.color = color;
				
		}

}
